resource "google_compute_instance" "demo_binar" {
    name = "terraform-binar-${count.index+1}"
    count = 1
    machine_type = "e2-micro"
    boot_disk {
      initialize_params {
        image = "ubuntu-os-cloud/ubuntu-2204-lts"
      }
    }
    metadata = {
        ssh-keys = "myusuf:${file("~/.ssh/myusuf@binar.pub")}"
    }
    tags = ["http-server"]
    network_interface {
      network = "default"
      access_config {

      }
    }
}

output "instance_ip" {
  value = google_compute_instance.demo_binar.0.network_interface.0.access_config.0.nat_ip
}