| Key               | Value                |
| ----------------- | -------------------- |
| Domain Staging    | staging.myusuf.me    |
| Domain Production | production.myusuf.me |
| Link Repository   | https://gitlab.com/myusuf-binar/platinum-challenge-binar    |





## Architecture Diagram



![image-20230409163932427](./assets/image-20230409163932427.png)





![image-20230409163948525](./assets/image-20230409163948525.png)

![image-20230409164002718](./assets/image-20230409164002718.png)



## Setup Kubernetes Cluster

- Register domain with static IP address (staging&production)
    - Using script `deployments/scripts/01-create_static_ip.sh`
- Create a Kubernetes cluster named `platinum-binar-yusuf-cluster` in the `asia-southeast2-a` zone.
    - Using script `deployments/scripts/02-create_cluster.sh`
- Connect to the Kubernetes cluster `gcloud container clusters get-credentials platinum-binar-yusuf-cluster --zone asia-southeast2-a --project fit-pathway-374702`
- Set up a database instance and create databases and users for both staging and production environments.
    - `cd deployments/terraform-cloud-sql && terraform apply`
- Seed or import the database.
- Update the environment variables `K8S_SECRET_STAGING_DATABASE_HOST` and `K8S_SECRET_PROD_DATABASE_HOST` with the new public IP of the database.  
- Update the environment variables `K8S_SECRET_STAGING_DATABASE_PASSWORD` and `K8S_SECRET_PROD_DATABASE_PASSWORD` with the new database password.
- Add gitlab runner in kubernetes cluster
    - `k apply -f deployments/helm-gitlab-runner/namespace.yaml`
    - `k apply -f deployments/helm-gitlab-runner/gitlab-secret.yaml -n gitlab`
    - `helm install --namespace gitlab gitlab-runner -f deployments/helm-gitlab-runner/values.yaml gitlab/gitlab-runner`
- Deploy Kubernetes via Gitlab CI/CD

## Setup Gitlab Runner

1. `cd deployments/gitlab-runner/terraform-launch-gcp-instance && terraform apply` 
2. Update inventory with new IP server address from terraform ouput
3. `cd deployments/gitlab-runner/ansible-gitlab-runner && ansible-playbook playbook.yml -i inventory.ini`

## Monitoring

### Kubernetes

Monitoring CPU, Memory Dll

![Monitoring CPU, Memory Dll](./assets/image-20230406094637221.png)

Logs

![image-20230406094658198](./assets/image-20230406094658198.png)



### Load Balancer



Monitoring 5xx & 4xx

![image-20230406094744061](./assets/image-20230406094744061.png)

![image-20230406094825011](./assets/image-20230406094825011.png)

Logs 

![image-20230406094923760](./assets/image-20230406094923760.png)
