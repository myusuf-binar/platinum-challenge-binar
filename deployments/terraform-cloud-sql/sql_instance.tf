resource "google_sql_database_instance" "binar_sql_instance" {
    name = "dbplatinumid"
    database_version = "POSTGRES_14"
    deletion_protection = false
    settings {
      tier = "db-f1-micro"
      availability_type = "ZONAL"
      disk_size = 10
      activation_policy = "ALWAYS"
      deletion_protection_enabled = false
      ip_configuration {
        ipv4_enabled = true
        authorized_networks {
          name = "internet"
          value = "0.0.0.0/0"
        }
      }
    }
}