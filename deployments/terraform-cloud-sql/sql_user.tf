resource "random_id" "random_user_platinumprod_password" {
    byte_length = 12
}

resource "random_id" "random_user_platinumstaging_password" {
    byte_length = 12
}

resource "google_sql_user" "platinumprod" {
    name = "platinumprod"
    password = random_id.random_user_platinumprod_password.hex
    instance = google_sql_database_instance.binar_sql_instance.name
}

resource "google_sql_database" "dbplatinumprod" {
    name = "dbplatinumprod"
    instance = google_sql_database_instance.binar_sql_instance.name
}

resource "google_sql_user" "platinumstaging" {
    name = "platinumstaging"
    password = random_id.random_user_platinumstaging_password.hex
    instance = google_sql_database_instance.binar_sql_instance.name
}

resource "google_sql_database" "dbplatinumstaging" {
    name = "dbplatinumstaging"
    instance = google_sql_database_instance.binar_sql_instance.name
}