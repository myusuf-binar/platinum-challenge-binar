output "binar_sql_user_platinumprod_password" {
    description = "Password of DB User platinumprod"
    value = random_id.random_user_platinumprod_password.hex
}

output "binar_sql_user_platinumstaging_password" {
    description = "Password of DB User platinumstaging"
    value = random_id.random_user_platinumstaging_password.hex
}

output "binar_sql_instance_name" {
    description = "Name of SQL instance"
    value = google_sql_database_instance.binar_sql_instance.name
}

output "binar_sql_instance_private_ip" {
    description = "Private IP of the SQL instance"
    value = google_sql_database_instance.binar_sql_instance.private_ip_address
}

output "binar_sql_instance_public_ip" {
    description = "Public IP of the SQL instance"
    value = google_sql_database_instance.binar_sql_instance.public_ip_address
}

