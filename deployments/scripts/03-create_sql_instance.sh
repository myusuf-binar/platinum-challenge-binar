gcloud sql instances create dbplatinumid --assign-ip \
  --authorized-networks=0.0.0.0/0 \
  --availability-type=zonal \
  --no-backup \
  --tier=db-f1-micro \
  --database-version=POSTGRES_14 \
  --no-deletion-protection \
  --enable-password-policy \
  --root-password=knd1k92aknfckndfa \
  --no-storage-auto-increase \
  --storage-size=10GB \
  --storage-type=SSD \
  --zone=asia-southeast2-a \
  --async


  # docs = https://cloud.google.com/sdk/gcloud/reference/sql/instances/create