kubectl create namespace gitlab
kubectl apply -f gitlab-secret.yaml -n gitlab
helm install --namespace gitlab gitlab-runner -f values.yaml gitlab/gitlab-runner